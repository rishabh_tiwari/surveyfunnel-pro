const { addFilter } = wp.hooks;
import React from 'react';

addFilter( 'scoringLogicCardFilter', 'ContentElements', scoringLogicCardFilter );
addFilter( 'conditionalLogicCardFilter', 'ContentElements', conditionalLogicCardFilter );

function scoringLogicCardFilter( data, item, type ) {
	if( type === 'scoring' ) {
		return <label style={{padding: '1px 6px'}}>Score: {item.startRange} to {item.endRange}</label>
	}
	return '';
}

function conditionalLogicCardFilter( data, conditionalLogicCard ) {
	return <button className="surveyfunnel-lite-cardBox-btn" onClick={conditionalLogicCard}><img src={require(`./BuildImages/logic-branch.png`)} /></button>
}

let exportFunctionsForTesting = { scoringLogicCardFilter };
export default exportFunctionsForTesting;