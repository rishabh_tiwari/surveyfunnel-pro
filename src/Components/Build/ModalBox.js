import ImageQuestion from "./BuildElements/ImageQuestion";
import TextElement from "./BuildElements/TextElement";
import ConditionalLogic from "./BuildElements/ConditionalLogic";

const { addFilter } = wp.hooks;

addFilter('getComponentRender', 'ModalBox', getComponentRender);

function getComponentRender(render, componentProps, componentName, ModalContentRight, CloseModal) {
	switch( componentName ) {
		case 'TextElement':
			return <TextElement {...componentProps} ModalContentRight={ModalContentRight} CloseModal={CloseModal} />
		case 'ImageQuestion':
			return <ImageQuestion {...componentProps} ModalContentRight={ModalContentRight} CloseModal={CloseModal} />
		case 'ConditionalLogic':
			return <ConditionalLogic {...componentProps} CloseModal={CloseModal}/>
		default:
			return '';
	}
}