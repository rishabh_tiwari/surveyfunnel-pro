import testFunctions from '../../../../Components/Build/BuildElements/StartScreenElements';

test('test cover page state by adding a privacy option to the react state', () => {
	let state = {
		title: 'Apartment Survey',
		description: 'Apartment Survey Description',
		button: 'Enter'
	};
	let receivedData = testFunctions.addCoverPageStateElements(state);
	expect(receivedData.title).toBe(state.title);
	expect(receivedData.description).toBe(state.description);
	expect(receivedData.button).toBe(state.button);
	expect(receivedData.privacyPolicy).toBe(false);
});

test( 'Start Screen Mount data', () => {
	let data = {};
	let currentElement = {
		privacyPolicy: true,
	}

	let receivedData = testFunctions.componentDidMountStartScreen(data, currentElement)
	expect(receivedData.privacyPolicy).toBeTruthy();

	currentElement = {};
	receivedData = testFunctions.componentDidMountStartScreen(data, currentElement);

	expect(receivedData.privacyPolicy).toBeFalsy();
	
} );

test( 'Start Screen Privacy Policy checkbox render in modalbox', () => {
	let data = '';
	let proSettings = {
		privacyPolicy: {
			text: 'something',
			link: {
				value: 'http://www.google.com'
			}
		}
	};
	let state = {
		privacyPolicy: true,
	}
	let jsx = testFunctions.startScreenRightPrivacyPolicy(data, state, proSettings);

	expect(jsx.props.children.type === 'div').toBeTruthy();
	state.privacyPolicy = false;
	jsx = testFunctions.startScreenRightPrivacyPolicy(data, state, proSettings);
	expect(jsx.props.children === '').toBeTruthy();

	proSettings.privacyPolicy.text = '';
	jsx = testFunctions.startScreenRightPrivacyPolicy(data, state, proSettings);
	expect(jsx.props.children === '').toBeTruthy();
} )

test( 'Start Screen privacy policy settings checkbox enabled or disabled', () => {
	let state = {
		privacyPolicy: true,
	}
	let proSettings = {
		privacyPolicy: {
			text: 'something',
			link: {
				value: 'http://www.google.com'
			}
		}
	};
	let handleChange = () => {}
	let jsx = testFunctions.startScreenLeftElements('', handleChange, state, proSettings);
	expect(jsx.props.children[1].props.checked).toBeTruthy();
	expect(jsx.props.children[1].props.disabled).toBeFalsy();

	state.privacyPolicy = false;
	jsx = testFunctions.startScreenLeftElements('', handleChange, state, proSettings);
	expect(jsx.props.children[1].props.checked).toBeFalsy();
	expect(jsx.props.children[1].props.disabled).toBeFalsy();

	proSettings.privacyPolicy.text = '';
	jsx = testFunctions.startScreenLeftElements('', handleChange, state, proSettings);
	expect(jsx.props.children[1].props.checked).toBeFalsy();
	expect(jsx.props.children[1].props.disabled).toBeTruthy();
} )