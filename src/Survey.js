const { addFilter } = wp.hooks;
import React from 'react';
import './scss/common.scss';
import './scss/imageQuestion.scss';

addFilter( 'renderPrivacyPolicyOption', 'Survey', renderPrivacyPolicyOption );

function validateImageUrl( url ) {
	return url.match(/^http.*\.(jpeg|jpg|png)$/) != null
}

function renderPrivacyPolicyOption (data, configure, item, src) {

	let disabled = false;
	if ( configure.proSettings?.privacyPolicy.text === '' || configure.proSettings?.privacyPolicy.link?.value === '' ) {
		disabled = true;
	}
	let privacyPolicy = item?.privacyPolicy && ! disabled;
	let privacyPolicyText = 'Privacy Policy';
	let privacyPolicyLink = '';
	if (configure?.proSettings) {
		privacyPolicyText = configure.proSettings.privacyPolicy.text;
		privacyPolicyLink = configure.proSettings.privacyPolicy.link.value;
	}
	return <>
		{privacyPolicy ? <div className="privacyPolicy">
			<p><input type="checkbox" id="privacyPolicyEnabled" /><label htmlFor="privacyPolicyEnabled">
					<span>
						<img src={src} alt="Privacy Policy Checkbox" />
					</span>
				</label> {privacyPolicyText} <a href={privacyPolicyLink} target="_blank">Learn More</a></p>
		</div> : ''}
	</>
}

addFilter( 'checkCoverPageButtonValidations', 'Survey', checkCoverPageButtonValidations );

function checkCoverPageButtonValidations(flag, item, iframeRef, error, configure) {
	let disabled = false;
	if ( configure.proSettings?.privacyPolicy.text === '' || configure.proSettings?.privacyPolicy.link?.value === '' ) {
		disabled = true;
	}
	let privacyPolicy = item?.privacyPolicy && ! disabled;

	if ( privacyPolicy ) {
		if ( iframeRef.current?.node?.contentDocument.getElementById('privacyPolicyEnabled').checked ) {
			return false;
		}
		error.push(<p key={error.length}>Kindly accept the Privacy Policy to proceed.</p>);
		return true;
	}

	return false;
}


addFilter('changeCurrentTabAsPerConditionalLogic', 'DesignPreview', changeCurrentTabAsPerConditionalLogic)

function changeCurrentTabAsPerConditionalLogic(num, componentList, currentTab) {
	if(componentList[currentTab].type === 'START_ELEMENTS') {
		return num;
	}
	let {conditions} = componentList[currentTab];
	if(!conditions) {
		return num;
	}
	const conditionsLength = conditions.length;
	const componentListLength = componentList.length;
	let currentIndex = 0;
	for(let i=0; i < componentListLength; i++) {
		if(componentList[currentTab].id === componentList[i].id) {
			currentIndex = i;
			break;
		}
	}
	switch(componentList[currentTab].componentName) {
		case 'SingleChoice':
		case 'ImageQuestion':
			for(let i=0; conditionsLength; i++) {
				if(!conditions[i]?.conditionOn) {
					return num;
				}
				if(conditions[i].conditionOn) {
					if(componentList[currentTab].value === conditions[i].jumpOption) {
						let jumpQuestionId = conditions[i].jumpQuestion.id;
						for(let j=0; j<componentListLength; j++) {
							if(componentList[j].id === jumpQuestionId) {
								if(j < currentIndex) {
									j = j-currentIndex;
								}
								return j;
							}
						}
					}
				}
			}
			break;
		case 'MultiChoice':
			let selectedAnswers = [];
			for(let i=0;i<componentList[currentTab].answers.length;i++) {
				if(componentList[currentTab].answers[i].checked) {
					selectedAnswers.push(componentList[currentTab].answers[i].name)
				}
			}
			if(selectedAnswers.length === 0) {
				return num;
			}
			for(let i=0; i<conditionsLength; i++) {
				if(!conditions[i]?.conditionOn) {
					return num;
				}
				if(conditions[i].jumpCondition === 'any') {
					for(let k=0; k < conditions[i].jumpOption.length; k++) {
						if(selectedAnswers.includes(conditions[i].jumpOption[k])) {
							for(let j=0; j<componentListLength; j++) {
								if(componentList[j].id === conditions[i].jumpQuestion.id) {
									if(j < currentIndex) {
										j = j-currentIndex;
									}
									return j;
								}
							}
						}
					}
				}else {
					let flag = 0;
					if(selectedAnswers.length === conditions[i].jumpOption.length) {
						for(let k=0; k<conditions[i].jumpOption.length; k++) {
							if(!selectedAnswers.includes(conditions[i].jumpOption[k])) {
								flag = 1;
								break;
							}
						}
						if(flag === 0) {
							for(let j=0; j<componentListLength; j++) {
								if(componentList[j].id === conditions[i].jumpQuestion.id) {
									if(j < currentIndex) {
										j = j-currentIndex;
									};
									return j;
								}
							}
						}
					}
				}
			}
			break;
		default: 
		for(let i=0; conditionsLength; i++) {
			if(!conditions[i]?.conditionOn) {
				return num;
			}
			if(conditions[i].conditionOn) {
				let jumpQuestionId = conditions[i].jumpQuestion.id;
				for(let j=0; j<componentListLength; j++) {
					if(componentList[j].id === jumpQuestionId) {
						if(j < currentIndex) {
							j = j-currentIndex;
						}
						return j;
					}
				}
			}
		}
		return num;
	}
	return num;
}

addFilter( 'changeCurrentTabAsPerSurveyType', 'Survey', changeCurrentTabAsPerSurveyType );

function changeCurrentTabAsPerSurveyType(num, surveyType, componentList, currentTab, globalTotalScore) {
	if ( surveyType === 'scoring' && componentList[currentTab].type !== 'RESULT_ELEMENTS' ) {
		let componentListLength = componentList.length;
		let resultStartLength = currentTab + num;
		let flag = false;
		
		// check whether currentab + 1 is resultElement
		if ( componentList[resultStartLength].type === 'RESULT_ELEMENTS' ) {
			let totalScore = calculateScore( componentList, currentTab );
			globalTotalScore.score = totalScore;

			// find result element which comes in range of that score.
			for ( let i = resultStartLength; i < componentListLength ; i++ ) {
				if ( totalScore >= componentList[i].startRange && totalScore <= componentList[i].endRange ) {
					flag = true;
					num = i;
					break;
				}
			}
			// if not found set it to last component list which is default.
			if ( ! flag ) {
				num = componentListLength - 1;
			}

			return num;
		}
		else {
			return num;
		}
	}
	return num;
}

function calculateScore( componentList, currentTab ) {
	// calculate score.
	let totalScore = 0;
	for(let i = 1; i <= currentTab ; i++) {
		if ( componentList[i].componentName !== 'SingleChoice' && componentList[i].componentName !== 'MultiChoice' && componentList[i].componentName !== 'ImageQuestion' ) {
			continue;
		}
		let tempScore = 0;
		let { answers } = componentList[i];
		
		switch( componentList[i].componentName ) {
			case 'SingleChoice':
			case 'ImageQuestion':
				for(let j = 0; j < answers.length ; j++) {
					if ( answers[j].name === componentList[i].value ) {
						tempScore = Number( answers[j].score );
					}
				}
				break;
			case 'MultiChoice':
				for(let j = 0; j < answers.length ; j++) {
					if ( answers[j].checked ) {
						tempScore += Number( answers[j].score );
					}
				}
				break;
			default:
				break;
		}
		totalScore += tempScore;
	}

	return totalScore;
}

addFilter( 'renderResultScreen', 'Survey' , renderResultScreen );

function renderResultScreen( data, item, surveyType, globalTotalScore ) {
	let style = {
		display: 'inline-block',
		marginBottom: "32px",
		padding: '20px 30px',
		boxShadow: '0 9px 14.2px 1.28px rgba(33,33,30,0.09)',
		background: 'white',
		fontSize: '20px',
		borderRadius: '12px',
	}
	if ( surveyType === 'scoring' ) {
		if ( item.showResult ) {
			return <div className="showResult" style={{...style}}>
				{item.displayMessage} <strong>{globalTotalScore.score}</strong>
			</div>
		}
		return '';
	}
	return '';
}

addFilter( 'renderContentElements', 'Survey', renderContentElements )

function renderContentElements( data, item, style, convertToRgbaCSS, changeCurrentTab, designCon, handleRadioChange, index, error, ShowErrors ) {
	switch( item.componentName ) {
		case 'TextElement':
			return (
				<div className={"surveyfunnel-lite-tab-" + item.componentName}
					style={{ ...style }}
					key={item.id}
				>
					<div
						className="tab-container"
						key={item.id}
					>
						<div className="tab" tab-componentname={item.componentName}>
							<h3 className="surveyTitle">{item.title}</h3>
							<p className="surveyDescription">{item.description}</p>
							<button type="button" className="surveyButton" style={{ background: convertToRgbaCSS(designCon.buttonColor), color: convertToRgbaCSS(designCon.buttonTextColor) }} onClick={() => {
									changeCurrentTab(1);
							}}>Next</button>
						</div>
					</div>
				</div>
			);
		case 'ImageQuestion':
			return (
				<div className={"surveyfunnel-lite-tab-" + item.componentName}
					style={{ ...style }}
					key={item.id}
				>
					<div
						className="tab-container"
						key={item.id}
					>
						<div className="tab" tab-componentname={item.componentName}>
							<h3 className="surveyTitle">{item.title}</h3>
							<p className="surveyDescription">{item.description}</p>
							<div className="imageAnswerFlexBox">
							{ item.answers.map(function(answer, idx) {
									return validateImageUrl(answer.imageUrl) ? (<label key={idx} style={{ border: `0.45px solid ${convertToRgbaCSS(designCon.answerBorderColor)}` }} className={"surveyfunnel-lite-tab-imageanswer-container" + ( item.value === answer.name ? ' active' : '' )}>
												<input id={`${idx}_${answer.name}_single`} type="radio" name="radiogroup" onChange={handleRadioChange} value={answer.name} listidx={index} />
												<div htmlFor={`${idx}_${answer.name}_single`} className="imageAnswerBox">
													<div className="image-answer">
														<img src={answer.imageUrl} alt={`image${idx}`} />
													</div>
													<div className='imageAnswerName'>
														{answer.name}
													</div>
												</div>
											</label>) : ''}
								)}
							</div>
							<ShowErrors error={error} />
							<button type="button" className="surveyButton" style={{ background: convertToRgbaCSS(designCon.buttonColor), color: convertToRgbaCSS(designCon.buttonTextColor) }} onClick={() => {
									changeCurrentTab(1);
							}}>Next</button>
						</div>
					</div>
				</div>
			)
		default:
			return '';
	}
}

addFilter( 'callRenderContentElements', 'Survey', callRenderContentElements );

function callRenderContentElements( data, renderElements, item, i ) {
	switch( item.componentName ) {
		case "TextElement":
		case "ImageQuestion":
			return renderElements(item, 'block', i);
		default:
			return '';
	}
}

addFilter( 'checkValidations', 'Survey', checkValidations );

function checkValidations(data, item, error) {
	
	switch( item.componentName ) {
		case "ImageQuestion":
			if ( item.mandatory && item.value === '' ) {
				error.push(
					<p key={error.length}>
						Please select atleast one answer!
					</p>
				)
			}
		default:
			break;
	}
}

let exportFunctionsForTesting = { renderPrivacyPolicyOption, checkCoverPageButtonValidations, calculateScore, renderResultScreen, changeCurrentTabAsPerSurveyType, renderContentElements, callRenderContentElements, checkValidations };
export default exportFunctionsForTesting;