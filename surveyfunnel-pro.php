<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://club.wpeka.com
 * @since             1.0.0
 * @package           Surveyfunnel_Pro
 *
 * @wordpress-plugin
 * Plugin Name:       SurveyFunnel Pro
 * Plugin URI:        https://club.wpeka.com/
 * Description:       Boost your leads using quiz / survey lead techniques . Drag & drop form builder to launch quizzes within minutes.
 * Version:           1.2.1
 * Author:            WPeka Club
 * Author URI:        https://club.wpeka.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       surveyfunnel
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SURVEYFUNNEL_PRO_VERSION', '1.2.1' );

if ( ! defined( 'SURVEYFUNNEL_PRO_PLUGIN_URL' ) ) {
    define('SURVEYFUNNEL_PRO_PLUGIN_URL', plugin_dir_url(__FILE__));
}

// Load WC_AM_Client class if it exists.
if ( ! class_exists( 'WPEWC_AM_Client_2_7' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'auto-updates/wc-am-client.php';
}

// Instantiate WC_AM_Client class object if the WC_AM_Client class is loaded.
if ( class_exists( 'WPEWC_AM_Client_2_7' ) ) {
    $srf_wcam_lib = new WPEWC_AM_Client_2_7( __FILE__, '', '1.2.1', 'plugin', 'https://club.wpeka.com/', 'SurveyFunnel Pro' );
}

require_once 'class-srftgm-plugin-activation.php';
add_action( 'tgmpa_register', 'surveyfunnel_pro_register_required_plugins' );

/**
 * Install required plugins for Pro Version.
 *
 * @since 7.0
 */
function surveyfunnel_pro_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        array(
            'name'               => 'SurveyFunnel Lite',
            'slug'               => 'surveyfunnel-lite',
            'version'            => '1.1.1',
            'required'           => true,
            'force_activation'   => true,
            'force_deactivation' => false,
        ),

    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id'           => 'surveyfunnel-lite',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'srftgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'plugins.php',            // Parent menu slug.
        'capability'   => 'manage_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.

    );
    srf_tgmpa( $plugins, $config );
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-surveyfunnel-pro-activator.php
 */
function activate_surveyfunnel_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-surveyfunnel-pro-activator.php';
	Surveyfunnel_Pro_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-surveyfunnel-pro-deactivator.php
 */
function deactivate_surveyfunnel_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-surveyfunnel-pro-deactivator.php';
	Surveyfunnel_Pro_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_surveyfunnel_pro' );
register_deactivation_hook( __FILE__, 'deactivate_surveyfunnel_pro' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-surveyfunnel-pro.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_surveyfunnel_pro() {

	$plugin = new Surveyfunnel_Pro();
	$plugin->run();

}
run_surveyfunnel_pro();
