<?php
/**
 * Class Test_Surveyfunnel_Pro
 *
 * @package Surveyfunnel_Pro
 * @subpackage Surveyfunnel_Pro/Tests
 */

/**
 * Unit test cases for class Surveyfunnel_Pro.
 *
 * @package    Surveyfunnel_Pro
 * @subpackage Surveyfunnel_Pro/Tests
 * @author     WPEka <hello@wpeka.com>
 */
class Test_Surveyfunnel_Pro extends WP_UnitTestCase {

	/**
	 * Surveyfunnel_Pro class instance.
	 *
	 * @access public
	 * @var string $surveyfunnel_Pro class instance.
	 */
	public static $surveyfunnel_pro;

	/**
	 * Set up function.
	 *
	 * @param class WP_UnitTest_Factory $factory class instance.
	 */
	public static function wpSetUpBeforeClass( WP_UnitTest_Factory $factory ) {
		self::$surveyfunnel_pro = new Surveyfunnel_Pro();
	}

	/**
	 * Test for constructor
	 */
	public function test_construct() {
		$obj = new Surveyfunnel_Pro();
		$this->assertTrue( $obj instanceof Surveyfunnel_Pro );
	}

	/**
	 * Test for get_plugin_name function
	 */
	public function test_get_plugin_name() {
		$plugin_name = self::$surveyfunnel_pro->get_plugin_name();
		$this->assertSame( 'surveyfunnel-pro', $plugin_name );
	}

	/**
	 * Test for get_version function
	 */
	public function test_get_version() {
		$version = self::$surveyfunnel_pro->get_version();
		$this->assertSame( '1.2.1', $version );
	}

	/**
	 * Test for get_loader function
	 */
	public function test_get_loader() {
		$loader = self::$surveyfunnel_pro->get_loader();
		$loader = (array) $loader;
		$it     = new RecursiveIteratorIterator( new RecursiveArrayIterator( $loader ) );
		$array  = array();
		foreach ( $it as $v ) {
			array_push( $array, $v );
		}

		// Test for some hooks.
		$this->assertTrue( in_array( 'plugins_loaded', $array, true ) );
		$this->assertTrue( in_array( 'surveyfunnel_lite_survey_page_html', $array, true ) );
	}

	/**
	 * Test for run function.
	 */
	public function test_run() {
		self::$surveyfunnel_pro->run();
		$this->assertTrue( true );
	}

}
